<?php

/**
 * Search API ranges data alteration callback that indexes the min and max
 * of selected numeric fields.
 */
class GeoclusterSolrFieldsAlter extends SearchApiAbstractAlterCallback {

  public function configurationForm() {
    // Retrieve indexed fields.
    $fields = $this->index->getFields(TRUE);
    $field_options = array();
    $this->options += array('fields' => array());
    $eligible_types = array('integer', 'decimal');
    foreach ($fields as $name => $field) {
      if (search_api_is_list_type($field['type']) && in_array(search_api_extract_inner_type($field['type']), $eligible_types))  {
        $field_options[$name] = $field['name'];
      }
    }
    if (!empty($field_options)) {
      $form['#attached']['css'][] = drupal_get_path('module', 'search_api') . '/search_api.admin.css';
      $form['fields'] = array(
        '#type' => 'checkboxes',
        '#title' => t('Fields to take stats off'),
        '#options' => $field_options,
        '#default_value' => $this->options['fields'],
        '#attributes' => array('class' => array('search-api-checkboxes-list')),
      );
      return $form;
    }
  }

  public function alterItems(array &$items) {
  	if (!$items) {
  		return;
  	}
  }
}
