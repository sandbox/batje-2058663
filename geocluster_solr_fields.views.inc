<?php

/**
 * Implements hook_views_data_alter().
 */
function geocluster_solr_fields_views_data_alter(&$data) {
	$fields = field_info_fields();
	$servers = search_api_server_load_multiple(FALSE);
	foreach (search_api_index_load_multiple(FALSE) as $index) {
		if (!empty($index->server) && !empty($servers[$index->server])) {
			$server = $servers[$index->server];
			if ($server->class == 'geocluster_solr_service') {
				$item = &$data['search_api_index_' . $index->machine_name];

				// We could add the field name as prefix but we don't for handler simplicity.
				$geocluster_field = 'geocluster_field';

				$solrfields = $index->server()->getFieldNames($index);

				$fields = $index->options['data_alter_callbacks']['geocluster_solr_fields_alter']['settings']['fields'];

				foreach ($fields as $field) {
					if ($field) {
						$item[$field] = array(
								'group' => 'Geocluster Solr Fields',
								// TODO We need the 'nice' name & description from the index here
								'title' => 'Geocluster ' . $field,
								'help' => 'Geocluster ' . $field,
								'field' => array(
										// TODO we need a special handler here that allows us to set the option for count/sum/min/max etc
										'handler' => 'geocluster_handler_field_numeric',
								),
						);
					}
				}
			}
		}
	}
}


